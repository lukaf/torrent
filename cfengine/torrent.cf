body common control {
    inputs => { "$(sys.workdir)/masterfiles/libraries/cfengine_stdlib.cf" };
}

# Install package from url.
bundle agent pkg_mgmt(name, url) {
    classes:
        "install_package" not => returnszero("/bin/rpm --quiet -q $(name)", "noshell"),
            handle => "check_if_package_exists",
            comment => "Check if package $(name) exists.";

    commands:
        install_package::
            "/usr/bin/yum -y install $(url)"
                handle => "install_package",
                comment => "Install package $(name)",
                classes => if_repaired("installed_package");

    reports:
        installed_package::
            "Installed $(name) from $(uri)";
}

bundle agent pip_install_module(name) {
    classes:
        "install_module" not => returnszero("/bin/pip list | grep $(name)", "useshell"),
            handle => "check_if_module_exists",
            comment => "Check if module $(name) exists";

    commands:
        install_module::
            "/bin/pip install $(name)"
                handle => "install_module",
                comment => "Install module $(name)",
                classes => if_repaired("installed_module");

    reports:
        installed_module::
            "Installed module $(name).";
}

bundle agent pip_install_requirements(file) {
    classes:
        "install_requirement" expression => fileexists("$(file)"),
            handle => "check_if_file_exists",
            comment => "Check if file exists.";

    commands:
        install_requirements::
            "/bin/pip install -r $(file)"
                handle => "install_requirements",
                comment => "Install requirements.",
                classes => if_repaired("installed_requirements");

    reports:
        installed_requirements::
            "Installed requirements from $(file)";
}

bundle agent main {
    methods:
        "install_nginx" usebundle => pkg_mgmt("nginx", "nginx");
        "install_pip" usebundle => pkg_mgmt("python-pip", "python-pip");
        "install_rtorrent" usebundle => pkg_mgmt("rtorrent", "rtorrent");
        "install_postfix" usebundle => pkg_mgmt("postfix", "postfix");
        "install_dovecot" usebundle => pkg_mgmt("dovecot", "dovecot");
        "install_nsd" usebundle => pkg_mgmt("nsd", "nsd");
        "install_git" usebundle => pkg_mgmt("git", "git");
}
