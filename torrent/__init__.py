import os


class Config(object):
    UPLOAD_DIR = os.path.expanduser('~/downloaded')
    DOWNLOAD_DIR = os.path.expanduser('~/torrents')
