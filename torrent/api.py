import re
import os
import uuid
from flask import Flask, jsonify, make_response, abort, request
from . import Config

content = {}

app = Flask(__name__)
app.config.from_object(Config)


def magnet_to_torrent(magnet_link):
    """
    Take magnet link and return content for a torrent file.
    """
    if re.search('xt=urn:btih', magnet_link):
        return "d10:magnet-uri{0:d}:{1:s}e".format(len(magnet_link), magnet_link)
    return False


def build_content():
    global content
    for path, _, files in os.walk(app.config['DOWNLOAD_DIR']):
        for f in files:
            full_path = os.path.join(path, f)
            content[str(uuid.uuid3(uuid.NAMESPACE_DNS, full_path))] = full_path
    return True


@app.route('/api/downloads', methods=['GET'])
def downloads():
    """
    Generates a JSON of found files with matching UUID.
    """
    build_content()
    return jsonify({'results': [content]})


@app.route('/api/download/<uid>', methods=['GET'])
def download(uid):
    """
    Check if the requested uid exists and pass on the needed info to Nginx.

    Nginx:
    location /files {
        internal;
        alias /downloaded;
    }

    GET /files/kekec.avi creates a request for /downloaded/files/kekec.avi
    """
    if len(content) == 0:
        build_content()
    if uid in content:
        response = make_response()
        response.headers['X-Accel-Redirect'] = content[uid]
        return response
    else:
        abort(404)


@app.route('/api/upload', methods=['POST'])
def upload():
    """
    Get magnet link data and save it to torrent file.
    Incoming data must be valid JSON with at least 'magnet' key:
    {'magnet': 'magnet-url'}
    """
    data = request.get_json()
    if not data:
        abort(501)

    magnet_link = data.get('magnet', None)
    if not magnet_link:
        abort(501)

    torrent = magnet_to_torrent(magnet_link)
    if torrent is False:
        abort(501)

    torrent_file = os.path.join(app.config['UPLOAD_DIR'], '{0:s}.torrent'.format(uuid.uuid4()))
    with open(torrent_file, 'w+') as f:
        f.write(torrent)

    return make_response(jsonify({'response': 'ok', 'data': magnet_link}))


@app.errorhandler(400)
@app.errorhandler(404)
@app.errorhandler(405)
@app.errorhandler(501)
def not_found(error):
    return make_response(jsonify({'error': error.description}), error.code)


if __name__ == '__main__':
    app.run(debug=True)
