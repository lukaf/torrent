[![Build Status](https://drone.io/bitbucket.org/lukaf/torrent/status.png)](https://drone.io/bitbucket.org/lukaf/torrent/latest)

Downloading huge files by passing the transport work from app to Nginx:
http://wiki.nginx.org/XSendfile

rtorrent encryption:
http://sohonetwork.blogspot.co.uk/2012/03/only-encrypted-connections-rtorrent.html

rtorrent general configuration:
http://libtorrent.rakshasa.no/wiki/RTorrentCommonTasks

flask unittests:
http://flask.pocoo.org/docs/testing/

flask + uwsgi + nginx:
http://flask.pocoo.org/docs/deploying/uwsgi/

vps:
https://cloud.digitalocean.com/login

flask cache:
http://pythonhosted.org/Flask-Cache/
