from __future__ import print_function
import unittest
import tempfile
import shutil
import uuid
import json

from torrent import api


class TorrentTest(unittest.TestCase):
    def setUp(self):
        self.magnet_link = 'magnet:?xt=urn:btih:892c905d865c9b4bf17eb99e2bb8557872a1c359&dn=Whiter+Posesion+Infernal+%282012%29+%5BHDrip-XviD-AC3%5D%5BCastellano%5D&tr=udp%3A%2F%2Ftracker.openbitapi.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337'
        self.content = 'd10:magnet-uri{0}:{1}e'.format(len(self.magnet_link), self.magnet_link)
        api.app.config['TESTING'] = True
        self.app = api.app.test_client()

    def tearDown(self):
        self.magnet_link = None
        self.content = None

    def test_magnet_to_torrent(self):
        self.assertEqual(
            api.magnet_to_torrent(self.magnet_link),
            self.content
        )
        self.assertFalse(api.magnet_to_torrent('not valid'))


class TorrentTestGET(unittest.TestCase):
    def setUp(self):
        self.temp_dir = tempfile.mkdtemp()
        _, self.temp_file = tempfile.mkstemp(dir=self.temp_dir)
        api.app.config['TESTING'] = True
        self.app = api.app.test_client()
        self.uuid = str(uuid.uuid3(uuid.NAMESPACE_DNS, self.temp_file))
        self.content = api.content

    def tearDown(self):
        shutil.rmtree(self.temp_dir)
        self.uuid = None
        self.temp_dir = None
        self.temp_file = None
        self.app = None
        self.content = None

    def test_downloads(self):
        api.app.config['DOWNLOAD_DIR'] = self.temp_dir
        response = self.app.get('/api/downloads')
        self.assertEquals(
            json.loads(response.data),
            {'results': [{self.uuid: self.temp_file}]}
        )
        api.content = self.content

    def test_download_nonexistent(self):
        response = self.app.get('/api/download/nonexistent')
        self.assertEquals(response.status_code, 404)

    def test_download_existent(self):
        api.content = {'42': '/answer'}
        response = self.app.get('/api/download/42')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('X-Accel-Redirect' in response.headers)
        self.assertEqual(response.headers['X-Accel-Redirect'], '/answer')
        api.content = self.content


class TorrentTestPOST(unittest.TestCase):
    def setUp(self):
        api.app.config['UPLOAD_DIR'] = tempfile.mkdtemp()
        api.app.config['TESTING'] = True
        self.app = api.app.test_client()
        self.magnet_link = 'magnet:?xt=urn:btih:892c905d865c9b4bf17eb99e2bb8557872a1c359&dn=Whiter+Posesion+Infernal+%282012%29+%5BHDrip-XviD-AC3%5D%5BCastellano%5D&tr=udp%3A%2F%2Ftracker.openbitapi.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337'
        self.content = 'd10:magnet-uri{0:d}:{1:s}e'.format(len(self.magnet_link), self.magnet_link)

    def tearDown(self):
        self.app = None
        self.magnet_link = None
        self.content = None
        shutil.rmtree(api.app.config['UPLOAD_DIR'])

    def test_upload_wrong_method(self):
        response = self.app.get('/api/upload')
        self.assertEqual(response.status_code, 405)
        self.assertTrue(json.loads(response.data))

    def test_upload_no_data(self):
        response = self.app.post('/api/upload')
        self.assertEqual(response.status_code, 501)
        self.assertTrue(json.loads(response.data))

    def test_upload_emtpy_data(self):
        response = self.app.post('/api/upload', data={})
        self.assertEqual(response.status_code, 501)
        self.assertTrue(json.loads(response.data))

    def test_upload_bad_data(self):
        response = self.app.post(
            '/api/upload',
            data=json.dumps({'magnet': 'bad data'}),
            headers={'Content-type': 'application/json'}
        )
        self.assertEqual(response.status_code, 501)
        self.assertTrue(json.loads(response.data))

    def test_upload_good_data(self):
        response = self.app.post(
            '/api/upload',
            data=json.dumps({'magnet': self.magnet_link}),
            headers={'Content-type': 'application/json'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(json.loads(response.data))
        data = json.loads(response.data)
        self.assertTrue(data['response'] == 'ok')
        self.assertEqual(data['data'], self.magnet_link)


if __name__ == '__main__':
    unittest.main()
